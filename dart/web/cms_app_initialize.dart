library kulturag.cms_app_init;

import 'package:angular/application_factory.dart';
import 'package:kulturag/cms/cms_root_scope.dart';

import 'package:kulturag/cms/cms_app_module.dart';
import 'package:logging/logging.dart';
import 'package:logging_handlers/logging_handlers_shared.dart';
import 'package:kulturag/global/logging_identifiers.dart';

final Logger _libLogger = new Logger("$LibraryID");

void main() {
  //init logging
  hierarchicalLoggingEnabled = true;
  Logger.root.onRecord.listen(new LogPrintHandler());

  Logger.root.level = Level.INFO;
  _libLogger.level = Level.ALL;
  //could be customized with _libLogger.level =  Level.INFO or Level.OFF and
  // then add specific logger
  //_logger.level = Level.All

// Hint for using injector:
// final injector = applicationFactory().addModule(new SpeedpadApp()).run();
  applicationFactory()
      .addModule(new CmsAppModule())
      .rootContextType(CmsRootScope)
      .run();
}

