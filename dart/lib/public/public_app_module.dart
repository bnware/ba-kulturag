library kulturag.public_app_module;

import 'package:angular/angular.dart';

import 'package:jb_menu/jb_menu.dart';


class PublicAppModule extends Module {

  PublicAppModule() {
    ///includes jb-menu-item, jb-menu, jb-responsive-breakpoints, jb-style
    install(new JbMenuModule());
  }

}