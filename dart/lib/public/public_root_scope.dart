library kulturag.root_scope;

import 'package:angular/angular.dart';


@Injectable()
class PublicRootScope {
  ///for jb-responsive-breakpoints
  String currentBreakpoint;

  String jbMenuStyles;
  String jbMenuItemStyles;
  List collapsedStateBreakpoints = ["small", "medium"];
}