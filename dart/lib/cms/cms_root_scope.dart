library kulturag.root_scope;

import 'package:angular/angular.dart';


@Injectable()
class CmsRootScope {
  String currentBreakpoint;
  String jbMenuStyles;
  String jbMenuItemStyles;
  List collapsedStateBreakpoints = ["none", "small"];
}