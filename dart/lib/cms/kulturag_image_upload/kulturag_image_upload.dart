library kulturag.image_upload_component;

import 'dart:html';
import 'dart:async';
import 'dart:convert';

import 'package:angular/angular.dart';
import 'package:logging/logging.dart';

@Component(
    selector: "image-upload-component[url]",
    templateUrl: "packages/kulturag/cms/kulturag_image_upload/kulturag_image_upload.html",
    useShadowDom: true
)
class ImageUploadComponent implements ShadowRootAware, AttachAware {

  Element _node;
  ShadowRoot _shadowRoot;
  FormElement _form;
  ProgressElement _progress;
  bool _testMode = false;
  Logger _logger = new Logger("kulturag.image_uplaod_component");

  File image;
  String error = "";
  String success = "";
  double progressValue = 0.0;
  String progressText = "";
  int progressMaxValue = 100;
  List<String> allowedImageTypes = ["image/gif", "image/png", "image/jpeg", "image/jpg"];

  @NgAttr("url")
  String url;

  @NgOneWayOneTime("src")
  String src;

  ImageUploadComponent(this._node) {
    _node.style
      ..display = "block";
  }


  @override
  void attach() {
    if (_node.attributes.containsKey("test-mode"))
      _testMode = true;
  }

  @override
  void onShadowRoot(ShadowRoot shadowRoot) {
    _shadowRoot = shadowRoot;
    _form = _shadowRoot.querySelector("#image-infos");
    _progress = _shadowRoot.querySelector("progress");

    if (!_testMode && url.isEmpty) {
      _logger.shout("Component runs not in test mode and url is empty!");
    }
  }

  upload() async {
    clearMessages();
    if (image == null) {
      error = "Bitte wählen Sie ein Bild aus.";
      return;
    }

    var data = new FormData(_form);
    data.appendBlob("file", image, image.name);

    HttpRequest request = await HttpRequest.request(url, method: "POST",
        withCredentials: true,
        sendData: data,
        requestHeaders: {"HTTP_X_REQUESTED_WITH":"XMLHttpRequest"},
        onProgress: (ProgressEvent e) {
          if (e.lengthComputable) {
            progressValue = (e.loaded / e.total) * 100;
            progressText = "${progressValue}%";
          }
        });

    var parsed = JSON.decode(request.response);
    if (parsed is int) {
      success = "Bild erfolgreich hochgeladen";
    } else if (parsed is String && parsed.startsWith("http")) {
      //URL case for redirect
    } else {
      //error
      error = request.response;
    }
  }

  clearMessages() {
    error = "";
    success = "";
  }
}