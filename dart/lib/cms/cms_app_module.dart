library kulturag.app;

import 'package:angular/angular.dart';

import 'package:bn_toggle/bn_toggle.dart';
import 'package:kulturag/cms/kulturag_image_upload/kulturag_image_upload.dart';
import 'package:jb_menu/jb_menu.dart';
import 'package:jb_image_import/jb_image_import.dart';
import 'package:bnware_generic_smooth_ui/bnware_generic_smooth_ui.dart';


class CmsAppModule extends Module {

  CmsAppModule() {
    //EXAMPLE: add event bus to dependency injection
    //bind(EventBus, toValue: new EventBus());

    //EXAMPLE: add storage service to dependency injection
    //    bind(StorageService);
    //    bind(StorageService, toValue: new StorageService());
    //    bind(StorageService, toFactory: (Angular.Injector inj) => new StorageService(inj.get(EventBus)));

    bind(BnToggle);

    bind(ImageUploadComponent);

    ///includes jb-menu-item, jb-menu, jb-responsive-breakpoints, jb-style
    install(new JbMenuModule());

    ///includes jb-image-input, jb-file-input, jb-bool-attr
    install(new JbImageImportModule());

    ///includes bn-smooth-button, bn-smooth-box
    install(new BNwareGenericSmoothUIComponents());
  }

}