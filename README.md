# Kultur-AG

This is a new beta version of the Kultur-AG website based on the CakePHP framework.
The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

On the client-side we use AngularDart and SASS compiled to Javascript and CSS.
The library source code can be found here: [AngularDart](https://github.com/angular/angular.dart).

## Installation

1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Change working directory to `cakephp`
3. Run `composer install`.
4. Configure at least the database connection (see Configuration) and read and run `cakephp/config/schema/kulturag.sql` to setup the database structure.
5. Run `bin/cake setup <email>` to setup the first admin user (in Windows `bin\cake setup <email>`).
6. Let the document root of the webserver point to `cakephp/webroot` or create a symlink to it.
   CakePHP is able to automatically detect the base path
7. Ensure URL-Rewriting is available and configurable via .htaccess file
8. Open `<base>/cms` in your browser and check if you can log in.
9. Your ready to manage content pages, pictures and members

## Configuration

Read and edit `cakephp/config/app.php` and setup the 'Datasources', 'Email' and 'EmailTransport' configuration.
You can also edit the values of the 'Pictures' section in `cakephp/config/kulturag.ini` to customize picture formats.
