-- Setupscript für Kultur-AG Webseite

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET @db = 'kulturag_test'; -- specify DB name here and in USE statement.


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `kulturag`
--
-- CREATE DATABASE IF NOT EXISTS @db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
SET @create = CONCAT('CREATE DATABASE IF NOT EXISTS `', @db, '` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci');
PREPARE stmt FROM @create;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
USE `kulturag_test`; -- specify DB name here.

--
-- Lösche alle Tabellen
--

-- Fake Tabelle anlegen, um Fehler bei leerer Datenbank zu vermeiden
CREATE TABLE IF NOT EXISTS `temp` (
  `id` int
);

SET FOREIGN_KEY_CHECKS = 0;
SET @tables = NULL;
SELECT GROUP_CONCAT(table_schema, '.', table_name) INTO @tables
FROM information_schema.tables
WHERE table_schema = @db;

SET @tables = CONCAT('DROP TABLE ', @tables);
PREPARE stmt FROM @tables;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `acl`
--

CREATE TABLE IF NOT EXISTS `acl` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) UNSIGNED NOT NULL,
  `aco_id` int(10) UNSIGNED NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  `_publish` char(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aco_id` (`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `acl`
--

INSERT INTO `acl` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`, `_publish`) VALUES
(1, 1, 1, '-1', '-1', '-1', '-1', '-1'),
(2, 3, 1, '1', '1', '1', '1', '1'),
(3, 5, 2, '1', '1', '1', '1', '1'),
(4, 5, 3, '-1', '-1', '-1', '-1', '-1'),
(5, 4, 2, '-1', '-1', '-1', '-1', '-1'),
(6, 4, 1, '1', '1', '1', '1', '1'),
(7, 6, 9, '1', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acos_lft_rght` (`lft`,`rght`),
  KEY `idx_acos_alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, '', NULL, 'ROOT', 1, 16),
(2, 1, '', NULL, 'Members', 2, 11),
(3, 2, '', NULL, 'Administrator', 3, 4),
(4, 2, '', NULL, 'Redakteur', 5, 6),
(5, 2, '', NULL, 'Mitgliederverwalter', 7, 8),
(6, 2, '', NULL, 'Mitglied', 9, 10),
(7, 1, '', NULL, 'Pictures', 12, 13),
(8, 1, '', NULL, 'Pages', 14, 15);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aros_lft_rght` (`lft`,`rght`),
  KEY `idx_aros_alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, '', NULL, 'ROOT', 1, 12),
(2, 1, '', NULL, 'Members', 2, 11),
(3, 2, '', NULL, 'Administrator', 3, 4),
(4, 2, '', NULL, 'Redakteur', 5, 6),
(5, 2, '', NULL, 'Mitgliederverwalter', 7, 8),
(6, 2, '', NULL, 'Mitglied', 9, 10);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `awards`
--

CREATE TABLE IF NOT EXISTS `awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `project_id` int(11) NOT NULL,
  `year` year(4) DEFAULT NULL,
  `picture_id` int(11) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `picture` (`picture_id`),
  KEY `project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Auszeichnungen';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `changes`
--

CREATE TABLE IF NOT EXISTS `changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(50) NOT NULL,
  `foreign_key` varchar(50) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifiedby` int(11) NOT NULL,
  `changes` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `changes` (`model`,`foreign_key`,`modified`,`modifiedby`) USING BTREE,
  KEY `model` (`model`,`foreign_key`) USING BTREE,
  KEY `member` (`model`,`foreign_key`,`modifiedby`),
  KEY `changes_modified_by` (`modifiedby`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='freizugebende Änderungen';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `avatar_id` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `password` binary(20) DEFAULT NULL,
  `ch_password` tinyint(1) NOT NULL DEFAULT '1',
  `show_email` varchar(50) DEFAULT NULL,
  `enquiries` tinyint(1) NOT NULL DEFAULT '0',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  KEY `name` (`lname`,`fname`),
  KEY `avatar` (`avatar_id`),
  KEY `role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Mitglieder';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(25) NOT NULL,
  `header` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page` (`parent_id`,`name`) USING BTREE,
  KEY `parent` (`parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Seiten';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `logo` (`logo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Partner';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pictures`
--

CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alt` varchar(50) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bilder';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('planned','running','archived') NOT NULL,
  `year` year(4) DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  `thumbnail_id` int(11) NOT NULL,
  `show_child_info` tinyint(1) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page` (`page_id`) USING BTREE,
  KEY `thumbnail` (`thumbnail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Projekte';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `project_members`
--

CREATE TABLE IF NOT EXISTS `project_members` (
  `project_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `ranking` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`member_id`),
  KEY `ranking` (`ranking`),
  KEY `member` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Projektmitglieder';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `project_partners`
--

CREATE TABLE IF NOT EXISTS `project_partners` (
  `project_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `ranking` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`partner_id`),
  KEY `ranking` (`ranking`),
  KEY `partner` (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Projektpartner';

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Rollen';

--
-- Daten für Tabelle `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Administrator'),
(2, 'Redakteur'),
(3, 'Mitgliederverwalter'),
(4, 'Mitglied');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Videos';

--
-- Constraints der Tabellen
--

--
-- Constraints der Tabelle `awards`
--
ALTER TABLE `awards`
  ADD CONSTRAINT `award_picture` FOREIGN KEY (`picture_id`) REFERENCES `pictures` (`id`),
  ADD CONSTRAINT `project_award` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);

--
-- Constraints der Tabelle `changes`
--
ALTER TABLE `changes`
  ADD CONSTRAINT `changes_modified_by` FOREIGN KEY (`modifiedby`) REFERENCES `members` (`id`);

--
-- Constraints der Tabelle `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `member_avatar` FOREIGN KEY (`avatar_id`) REFERENCES `pictures` (`id`),
  ADD CONSTRAINT `member_role` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints der Tabelle `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `parent_page` FOREIGN KEY (`parent_id`) REFERENCES `pages` (`id`);

--
-- Constraints der Tabelle `partners`
--
ALTER TABLE `partners`
  ADD CONSTRAINT `partner_logo` FOREIGN KEY (`logo_id`) REFERENCES `pictures` (`id`);

--
-- Constraints der Tabelle `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `project_page` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  ADD CONSTRAINT `project_thumbnail` FOREIGN KEY (`thumbnail_id`) REFERENCES `pictures` (`id`);

--
-- Constraints der Tabelle `project_members`
--
ALTER TABLE `project_members`
  ADD CONSTRAINT `member` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `mproject` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);

--
-- Constraints der Tabelle `project_partners`
--
ALTER TABLE `project_partners`
  ADD CONSTRAINT `partner` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`),
  ADD CONSTRAINT `project` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
