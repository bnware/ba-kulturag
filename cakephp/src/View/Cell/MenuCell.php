<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * Menu cell
 */
class MenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $Pages = $this->loadModel('Pages');

        $current = $this->request->param('pass')[0];
        $menu = $Pages->find('menu', ['current' => $current])->toArray();

        $menu += [
        ];
        $this->set(compact('menu'));
    }
}
