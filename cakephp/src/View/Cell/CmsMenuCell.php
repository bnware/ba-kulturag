<?php
namespace App\View\Cell;

use Cake\View\Cell;

/**
 * CmsMenu cell
 */
class CmsMenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method
     *
     * @param null $user
     */
    public function display($user = null)
    {
        $menu = [
            'Seiten' => [
                'url' => ['controller' => 'Pages', 'action' => 'index'],
                'active' => ($this->request->controller == 'Pages')
            ],
             'Bilder' => [
                'url' => ['controller' => 'Pictures', 'action' => 'index'],
                'active' => ($this->request->controller == 'Pictures')
            ],
            'Mitglieder' => [
                'url' => ['controller' => 'Members', 'action' => 'index'],
                'active' => ($this->request->controller == 'Members' && $this->request->action != 'password')
            ],
            ' ' => [
                'url' => "",
                'active' => false
            ],
            'Angemeldet als ' . $user['name'] => [
                'url' => "",
                'active' => false
            ],
            'Passwort ändern' => [
                'url' => ['controller' => 'Members', 'action' => 'password'],
                'active' => ($this->request->controller == 'Members' && $this->request->action == 'password'),
            ],
            'Abmelden' => [
                'url' => ['controller' => 'Members', 'action' => 'logout'],
                'active' => false
            ]
        ];
        $this->set(compact('menu'));
    }
}
