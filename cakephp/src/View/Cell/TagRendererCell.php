<?php

namespace App\View\Cell;

use BBCode\Exception\InvalidAttributeException;
use BBCode\Exception\MissingAttributeException;
use BBCode\Exception\TagRenderException;
use BBCode\Lib\TagData;
use BBCode\View\Cell\TagRendererTrait;
use Cake\Core\Configure;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Validation\Validation;
use Cake\View\Cell;

class TagRendererCell extends Cell {

    use TagRendererTrait;

    static private $_inlineTags = ['f', 'k', 'u', 'fett', 'kursiv', 'unterstrichen', 'hoch', 'tief', 'link', 'email'];

    static private $_alignments = [
        'links' => 'left',
        'rechts' => 'right',
        'zentriert' => 'center',
        'blocksatz' => 'justify'
    ];

    public function f (TagData $data) {
        $this->_passContent($data);
    }

    public function k (TagData $data) {
        $this->_passContent($data);
    }

    public function u (TagData $data) {
        $this->_passContent($data);
    }

    public function fett (TagData $data) {
        $this->viewBuilder()->template('f');
        $this->f($data);
    }

    public function kursiv (TagData $data) {
        $this->viewBuilder()->template('k');
        $this->k($data);
    }

    public function unterstrichen (TagData $data) {
        $this->viewBuilder()->template('u');
        $this->u($data);
    }

    public function hoch (TagData $data) {
        $this->_passContent($data);
    }

    public function tief (TagData $data) {
        $this->_passContent($data);
    }

    public function email (TagData $data) {
        if (!Validation::email($data->content)) {
            throw new TagRenderException('Ungültige Email-Adresse in tag [email]');
        }
        $this->set('email', $data->content);
    }

    public function link (TagData $data) {
        $this->_assertRequired($data, 'url');
        $this->set('url', $data->attributes->url);
        $this->_passContent($data);
    }

    public function absatz (TagData $data) {
        $this->_validateParent($data, self::$_inlineTags, false);
        $data->attributes->einzug = @$data->attributes->einzug ?: '0';
        $this->_validateAttribute($data, 'ausrichtung', array_keys(self::$_alignments), 'links');
        if (preg_match('/^(\d+(?:\.\d+)?)(px|pt|em|rem|%)?$/', $data->attributes->einzug, $parts)) {
            if (empty($parts[2])) {
                $data->attributes->indent = $parts[1] . 'rem';
            }
        } else {
            throw new InvalidAttributeException(['einzug', 'absatz', 'Der Wert muss eine gültige Zahl sein, optional mit einer der folgenden Einheiten: px, pt, em, rem, %']);
        }
        $this->set('style', ['text-align' => self::$_alignments[$data->attributes->ausrichtung], 'margin-left' => $data->attributes->einzug]);
        $this->_passContent($data);
    }

    public function bild (TagData $data) {
        $this->_assertStandalone($data);
        $this->_assertRequired($data, 'id');
        try {
            $Pictures = $this->loadModel('Pictures');
            $this->set('picture', $Pictures->get($data->attributes->id));
        } catch (RecordNotFoundException $e) {
            throw new InvalidAttributeException(['id', 'bild', 'Das Bild #' . $data->attributes->id . 'existiert nicht']);
        }
        $config = Configure::read('Pictures.thumbnail');
        $this->set('thumb', sprintf('%s/%u.%s', $config['path'], $data->attributes->id, $config['format']));
        $config = Configure::read('Pictures.small');
        $base = Configure::read('App.imageBaseUrl');
        $this->set('url', sprintf('/%s%s/%u.%s', $base, $config['path'], $data->attributes->id, $config['format']));
    }

}