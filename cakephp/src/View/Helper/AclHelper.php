<?php
namespace App\View\Helper;

use Acl\Controller\Component\AclComponent;
use Cake\View\Helper;
use Cake\View\View;

/**
 * Acl helper
 */
class AclHelper extends Helper
{

    public function check ($aro, $aco, $action = '*') {
        return $this->config('component')->check($aro, $aco, $action);
    }

}
