<?php $this->assign('title', $title); ?>
<p>Guten Tag <?= $name ?>, <p>
<p>
    Ihr Zugang zum Administrieren der Kultur-AG Webseite wurde angelegt. <br>
    Die Zugangsdaten sind:
</p>
<p>
    E-Mail Adresse: <?= $email ?> <br>
    Startpasswort: <?= $password ?>
</p>
<p>
    Bitte klicken Sie auf folgenden Link, um sofort Ihr Passwort zu ändern. <br>
    <?= $this->Html->link(['controller' => 'Members', 'action' => 'password', 'prefix' => 'cms', $token, '_full' => true, '_ssl' => true]) ?>
</p>
<p>
    Alternativ können Sie sich auch direkt auf
    <?= $this->Html->link(['controller' => 'Members', 'action' => 'login', 'prefix' => 'cms', '_full' => true, '_ssl' => true]) ?>
    anmelden und Ihr Passwort ändern.
</p>
<p>
    Mit freundlichen Grüßen,
</p>
<p>
    KulturAG CMS
</p>