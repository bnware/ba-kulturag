<?php $this->assign('title', $title); ?>
<p>Guten Tag <?= $name ?>, <p>
<p>
    Sie haben ein Zurücksetzen Ihres Passworts angefordert.
</p>
<p>
    Bitte klicken Sie auf folgenden Link, um ein neues Passwort zu vergeben.<br>
    <?= $this->Html->link(['controller' => 'Members', 'action' => 'password', 'prefix' => 'cms', $token, '_full' => true, '_ssl' => true]) ?>
</p>
<p>
    Falls Sie das Zurücksetzen Ihres Passworts nicht angefordert haben, ignorieren Sie diese Mail einfach.
</p>
<p>
    Mit freundlichen Grüßen,
</p>
<p>
    KulturAG CMS
</p>