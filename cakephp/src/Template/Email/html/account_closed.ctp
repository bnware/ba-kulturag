<?php $this->assign('title', $title); ?>
<p>Guten Tag <?= $name ?>, <p>
<p>
    Ihr Zugang zum KulturAG CMS wurde deaktiviert.
    Es ist keine weitere Aktion erforderlich.
</p>
<p>
    Mit freundlichen Grüßen,
</p>
<p>
    KulturAG CMS
</p>