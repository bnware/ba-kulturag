Guten Tag <?= $name ?>,

Sie haben ein Zurücksetzen Ihres Passworts angefordert.

Mit folgendem Link können Sie ein neues Passwort vergeben.<br>
<?= $this->Url->build(['action' => 'password', 'prefix' => 'cms', $token, '_full' => true, '_ssl' => true]) ?>

Falls Sie das Zurücksetzen Ihres Passworts nicht angefordert haben, ignorieren Sie diese Mail einfach.

Mit freundlichen Grüßen,

KulturAG CMS