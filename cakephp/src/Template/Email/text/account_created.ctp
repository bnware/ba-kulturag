Guten Tag <?= $name ?>,

Ihr Zugang zum Administrieren der Kultur-AG Webseite wurde angelegt.
Die Zugangsdaten sind:

E-Mail Adresse: <?= $email ?>
Startpasswort: <?= $password ?>

Mit folgendem Link können Sie Ihr Passwort sofort ändern.
<?= $this->Url->build(['action' => 'password', 'prefix' => 'cms', $token, '_full' => true, '_ssl' => true]) ?>

Alternativ können Sie sich auch direkt auf <?= $this->Url->build(['action' => 'login', 'prefix' => 'cms', '_full' => true, '_ssl' => true]) ?> anmelden und Ihr Passwort ändern.

Mit freundlichen Grüßen,

KulturAG CMS