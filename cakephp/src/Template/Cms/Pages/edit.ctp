<?php
$this->assign('title', 'Seitenverwaltung');
$this->assign('titleDisabled', true);


$this->start("navButtons");
?>
<bn-smooth-button ng-cloak>
    <?= $this->Html->link('Zurück zur Seitenverwaltung', ['action' => 'index']) ?>
</bn-smooth-button>
<?php

$this->end();

$this->start('css', ['block' => true]);
?>
<style type="text/css">
    fieldset {
        min-width: 32em;
    }
</style>
<?php $this->end(); ?>
<div class="form content columns">
    <?= $this->Form->create($page) ?>
    <bn-smooth-box ng-cloak style="width: 570px">
        <fieldset>
            <legend class="big-font">Seite bearbeiten</legend>
            <?php
            echo $this->Form->input('parent_id', ['options' => $parentPages, 'empty' => '/', 'label' => 'Übergeordnete Seite']);
            echo $this->Form->input('name');
            echo $this->Form->input('header', ['label' => 'Überschrift']);
            echo $this->Form->input('label', ['label' => 'Linkbeschriftung']);
            echo $this->Form->input('content', ['label' => 'Inhalt']);
            ?>
        </fieldset>
        <?= $this->Form->button('Speichern', ['type' => 'submit']) ?>
    </bn-smooth-box>
    <?= $this->Form->end() ?>
</div>
