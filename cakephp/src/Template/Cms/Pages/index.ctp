<?php
$this->assign('title', 'Seitenverwaltung');
$this->assign('titleDisabled', true);

$aro = ['Members' => $user];

$this->start("navButtons");
?>
<?php if ($this->Acl->check($aro, 'ROOT/Pages', 'create')): ?>
    <bn-smooth-button ng-cloak>
        <?= $this->Html->link('Neue Seite anlegen', ['action' => 'add']) ?>
    </bn-smooth-button>
<?php endif;
$this->end();
?>

<div class="pages index large-12 content">
    <h3>Seiten</h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('Acos.lft', 'URL') ?></th>
            <th><?= $this->Paginator->sort('Pages.header', 'Überschrift') ?></th>
            <th><?= $this->Paginator->sort('Pages.label', 'Linkbeschriftung') ?></th>
            <th class="actions">Aktionen</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($pages as $page):
            $url = h(str_replace('ROOT/Pages', '', $page->alias));
            $actions = [];
            if ($this->Acl->check($aro, $page, 'update')) {
                $actions[] = $this->Html->link('Bearbeiten', ['action' => 'edit', $page->id]);
            }
            if ($this->Acl->check($aro, $page, 'delete')) {
                $actions[] = $this->Form->postLink('Löschen',
                                                   ['action' => 'delete', $page->id],
                                                   ['confirm' => 'Wollen Sie die Seite "' . $url . '" wirklich löschen?']);
            }
            ?>
            <tr>
                <td><?= $url ?></td>
                <td><?= h($page->header) ?></td>
                <td><?= h($page->label) ?></td>
                <td class="actions">
                    <?= implode('<br>', $actions) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('<') ?>
            <?= $this->Paginator->numbers(['first' => 2, 'last' => 2]) ?>
            <?= $this->Paginator->next('>') ?>
        </ul>
        <p><?= $this->Paginator->counter('Seite {{page}} von {{pages}}') ?></p>
    </div>
</div>
