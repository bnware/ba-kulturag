<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        KulturAG CMS - <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('cms/cms_kulturag.css') ?>

    <?= $this->Html->script('/packages/web_components/webcomponents.min.js') ?>

    <!-- async modifier -->
    <?= $this->Html->script(['/cms_app_initialize.dart.js', '/packages/browser/dart.js'], ["async" => true]) ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <base href="<?= str_replace('//', '/', $this->request->base . '/') ?>">

    <style>
        .centered-box {
            display: flex;
            justify-content: center;
            align-items: center;
        }
    </style>
</head>
<body class="three-stripes-layout">
<jb-responsive-breakpoints current-breakpoint="currentBreakpoint"></jb-responsive-breakpoints>

    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>

    <?= $this->fetch('content') ?>

</body>
</html>
