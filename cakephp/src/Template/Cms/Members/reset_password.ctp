<?php $this->assign('title', 'Passwort vergessen'); ?>
<style>
    .centered-box bn-smooth-box {

    }
</style>

<div class="centered-box form">
    <bn-smooth-box class="ng-cloak">
        <?= $this->Form->create() ?>
        <fieldset>
            <legend class="big-font">Passwort vergessen</legend>
            <p>
                Um ein Zurücksetzen Ihres Passwortes anzufordern, geben Sie bitte Ihre Email-Adresse ein, mit der Sie
                sich anmelden.
                Daraufhin wird Ihnen eine Email mit einem Link zugeschickt.
                Bitte beachten Sie, dass dieser Link nur für eine Stunde gültig ist.
            </p>
            <?= $this->Form->input('email', ['required' => true]) ?>
            <?= $this->Form->button('Email anfordern', ['type' => 'submit']); ?>
            <?= $this->Html->link('Abbrechen', ['action' => 'login'], ['class' => 'button']) ?>
        </fieldset>
        <?= $this->Form->end() ?>
    </bn-smooth-box>
</div>
