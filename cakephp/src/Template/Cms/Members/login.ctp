<?php $this->assign('title', 'Login'); ?>
<style>

    .centered-box bn-smooth-box {
        min-width: 400px;
    }

    .margin {
        margin-right: 8px;
    }
</style>

<div class="form centered-box">
<!--    TODO: remove padding from smooth box with padding="0" - fix problems - not working -->
    <bn-smooth-box ng-cloak padding="0">
        <?= $this->Form->create() ?>
        <fieldset>
            <legend class="big-font"> KulturAG CMS - <?= $this->fetch('title') ?></legend>
            <?= $this->Form->input('email', ['required' => true]) ?>
            <?= $this->Form->input('password', ['label' => 'Passwort', 'required' => true]) ?>
            <?= $this->Html->link('Passwort vergessen?', ['action' => 'resetPassword'], ['class' => 'button margin']) ?>
            <?= $this->Form->button('Login', ['type' => 'submit']); ?>
        </fieldset>
        <?= $this->Form->end() ?>
    </bn-smooth-box>
</div>
