<?php
$this->assign('title', 'Mitgliederverwaltung');
$this->assign('titleDisabled', true);

$this->start("navButtons");
?>
<bn-smooth-button ng-cloak>
    <?= $this->Html->link('Zurück zur Mitgliederverwaltung', ['action' => 'index']) ?>
</bn-smooth-button>
<bn-smooth-button ng-cloak>
    <?= $this->Html->link('Weiteres Mitglied anlegen', []) ?>
</bn-smooth-button>
<?php

$this->end();

$this->start('css', ['block' => true]);
?>
<style type="text/css">
    fieldset {
        min-width: 32em;
    }
</style>
<?php $this->end(); ?>
<div class="form content">

    <?= $this->Form->create($member) ?>
    <bn-smooth-box ng-cloak style="width: 35em">
        <fieldset>
            <legend class="big-font">Mitglied anlegen</legend>
            <?= $this->Form->input('fname', ['label' => 'Vorname:']) ?>
            <?= $this->Form->input('lname', ['label' => 'Nachname:']) ?>
            <!-- TODO: add picture-chooser for avatar -->
            <? //= $this->Form->input('avatar') ?>
            <?= $this->Form->input('email', ['label' => 'Email-Adresse:']) ?>
            <?= $this->Form->hidden('role_id', ['default' => 4]) ?>
            <?= $this->Form->input('chk_show_email', ['type' => 'checkbox', 'label' => 'Alternative Email-Adresse auf Profilseite', 'bn-toggle' => 'chk_email_toggle']) ?>
            <fieldset id="chk_email_toggle" class="bn-toggle">
                <?= $this->Form->input('show_email', ['label' => 'Alternative Email-Adresse', 'type' => 'email']) ?>
            </fieldset>
            <?= $this->Form->input('cms', ['type' => 'checkbox', 'label' => 'CMS-Zugang gewähren', 'bn-toggle' => 'cms_toggle']) ?>
            <fieldset id="cms_toggle" class="bn-toggle">
                <?= $this->Form->input('role_id', ['label' => 'Rolle', 'options' => $roles, 'default' => 4]) ?>
                <?= $this->Form->input('enquiries', ['label' => 'Kontaktanfragen empfangen']) ?>
            </fieldset>
            <?= $this->Form->button('Anlegen', ['type' => 'submit']) ?>
        </fieldset>
    </bn-smooth-box>
    <?= $this->Form->end() ?>
</div>
