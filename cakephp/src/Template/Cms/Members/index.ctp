<?php
$this->assign('title', 'Mitgliederverwaltung');
$this->assign('titleDisabled', true);

$aro = ['Members' => $user];
$this->start("navButtons");
if ($this->Acl->check($aro, 'ROOT/Members', 'create')): ?>
    <bn-smooth-button ng-cloak>
        <?= $this->Html->link('Neues Mitglied anlegen', ['action' => 'add']) ?>
    </bn-smooth-button>
    <?php
endif;
$this->end();
?>
<div class="members index large-12 content">
    <h3>Mitglieder</h3>
    <?= $this->fetch('pagination') ?>
    <table cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="150" span="2">
            <col span="2">
            <col width="80">
            <col width="150">
            <col width="150">
            <col width="100">
        </colgroup>
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('fname', 'Vorname') ?></th>
            <th><?= $this->Paginator->sort('lname', 'Nachname') ?></th>
            <th><?= $this->Paginator->sort('email') ?></th>
            <!--                <th>Avatar</th>-->
            <th><?= $this->Paginator->sort('show_email', 'Profil-Email') ?></th>
            <th>CMS-Zugang</th>
            <th>Rolle</th>
            <th>Erhält Kontaktanfragen</th>
            <th class="actions">Aktionen</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($members as $member):
            $actions = [];
            if ($this->Acl->check($aro, $member, 'update')) {
                $actions[] = $this->Html->link('Bearbeiten', ['action' => 'edit', $member->id]);
            }
            if ($this->Acl->check($aro, $member, 'delete')) {
                $actions[] = $this->Form->postLink('Löschen',
                    ['action' => 'delete', $member->id],
                    ['confirm' => 'Wollen Sie das Mitglied "' . $member->name . '" wirklich löschen?']);
            }
            ?>
            <tr>
                <td><?= h($member->fname) ?></td>
                <td><?= h($member->lname) ?></td>
                <td><?= $this->Html->link($member->email, 'mailto:' . $member->email) ?></td>
                <!--                <td>-->
                <? //= isset($member->avatar) ? $this->Html->image($member->avatar . '.png', ['alt' => $member->name]) : ''
                ?><!--</td>-->
                <td><?= $this->Html->link($member->profile_email, 'mailto:' . $member->profile_email) ?></td>
                <td><?= $member->cms ? 'Ja' : 'Nein' ?></td>
                <td><?= h($member->role->name) ?></td>
                <td><?= $member->enquiries ? 'Ja' : 'Nein' ?></td>
                <td class="actions">
                    <?= implode('<br>', $actions) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('<') ?>
            <?= $this->Paginator->numbers(['first' => 2, 'last' => 2]) ?>
            <?= $this->Paginator->next('>') ?>
        </ul>
        <p><?= $this->Paginator->counter('Seite {{page}} von {{pages}}') ?></p>
    </div>
</div>
