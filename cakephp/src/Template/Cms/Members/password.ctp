<?php $this->assign('title', 'Passwort ändern'); ?>
<?php $this->assign('titleDisabled', true); ?>
<div class="form content columns">
    <?= $this->Form->create($member) ?>
    <bn-smooth-box ng-cloak style="width: 35em">
        <fieldset>
            <legend class="big-font">Passwort ändern</legend>
            <?php if (!$token): ?>
                <?= $this->Form->hidden('email') ?>
                <?= $this->Form->input('password', ['label' => 'aktuelles Passwort', 'required' => true, 'value' => '']) ?>
            <?php endif; ?>
            <?= $this->Form->input('new', ['label' => 'neues Passwort', 'type' => 'password', 'required' => true]) ?>
            <?= $this->Form->input('wdh', ['label' => 'Passwort wiederholen', 'type' => 'password', 'required' => true]) ?>
            <?= $this->Form->button('Speichern', ['type' => 'submit']); ?>
        </fieldset>
    </bn-smooth-box>
    <?= $this->Form->end() ?>
</div>
