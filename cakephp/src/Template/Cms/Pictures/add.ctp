<?php
$this->assign('title', 'Bilderverwaltung');
$this->assign('titleDisabled', true);

$this->start("navButtons");
?>
<bn-smooth-button ng-cloak>
    <?= $this->Html->link('Zurück zur Bilderverwaltung', ['action' => 'index']) ?>
</bn-smooth-button>
<bn-smooth-button ng-cloak>
    <?= $this->Html->link('Weiteres Bild hochladen', []) ?>
</bn-smooth-button>
<?php

$this->end();
?>

<div class="pictures form columns content">
    <bn-smooth-box ng-cloak style="width: 570px">
        <fieldset>
            <legend class="big-font">Bild hochladen</legend>
            <div style="display: flex;flex-flow: row nowrap;justify-content: center">
                <image-upload-component
                    url="<?= $this->URL->build(['action' => 'upload']) ?>">
                </image-upload-component>
            </div>
        </fieldset>
    </bn-smooth-box>

</div>
