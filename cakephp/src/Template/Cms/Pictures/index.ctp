<?php
use Cake\Core\Configure;
$this->assign('title', 'Bilderverwaltung');
$this->assign('titleDisabled', true);

$aro = ['Members' => $user];
$config = Configure::read('Pictures.thumbnail');
$this->start("navButtons");
if ($this->Acl->check($aro, 'ROOT/Pictures', 'create')):
?>
<bn-smooth-button ng-cloak>
    <?= $this->Html->link('Neues Bild hochladen', ['action' => 'add']) ?>
</bn-smooth-button>
<?php
endif;
$this->end();
?>
<div class="pictures index large-12 content">
    <h3>Bilder</h3>
    <table cellpadding="0" cellspacing="0">
        <colgroup>
            <col width="320">
            <col>
            <col span="2" width="75">
            <col width="50">
            <col width="100">
        </colgroup>
        <thead>
            <tr>
                <th>Vorschaubild</th>
                <th><?= $this->Paginator->sort('alt', 'Alternativtext') ?></th>
                <th><?= $this->Paginator->sort('width', 'Breite') ?></th>
                <th><?= $this->Paginator->sort('height', 'Höhe') ?></th>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th class="actions">Aktionen</th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach ($pictures as $picture):
            $actions = [];
            if ($this->Acl->check($aro, $picture, 'delete')) {
                $actions[] = $this->Form->postLink('Löschen',
                                                   ['action' => 'delete', $picture->id],
                                                   ['confirm' => 'Wollen Sie das Bild "' . $picture->alt . '" wirklich löschen?']);
            }
            $url = sprintf('%s/%u.%s', $config['path'], $picture->id, $config['format']);
            ?>
            <tr>
                <td><?= $this->Html->image($url, ['alt' => $picture->alt, 'title' => '', 'width' => '300']) ?></td>
                <td><?= h($picture->alt) ?></td>
                <td><?= $this->Number->format($picture->width) ?> px</td>
                <td><?= $this->Number->format($picture->height) ?> px</td>
                <td><?= $this->Number->format($picture->id) ?></td>
                <td class="actions">
                    <?= implode('<br>', $actions) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('<') ?>
            <?= $this->Paginator->numbers(['first' => 2, 'last' => 2]) ?>
            <?= $this->Paginator->next('>') ?>
        </ul>
        <p><?= $this->Paginator->counter('Seite {{page}} von {{pages}}') ?></p>
    </div>
</div>
