<jb-style content="jbMenuStyles">
    nav {
    }
    nav.collapsed {}
</jb-style>
<jb-style content="jbMenuItemStyles">
    a {
    color: #ababab;
    }
    a:hover, .active {
    color: rgb(230, 230, 230);
    }
    a.collapsed {}
</jb-style>
<jb-menu current-breakpoint="currentBreakpoint"
         collapsed-state-breakpoints="collapsedStateBreakpoints"
         style-transclude="jbMenuStyles">
    <?php foreach ($menu as $label => $menuItem): ?>
        <jb-menu-item
            url="<?= $this->Url->build($menuItem['url']) ?>"
            style-transclude="jbMenuItemStyles"
            class="ng-cloak"<?= $menuItem['active'] ? ' active' : '' ?>>
            <?= $label ?>
        </jb-menu-item>
    <?php endforeach; ?>
</jb-menu>
