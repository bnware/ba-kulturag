<?php

namespace App\Utility;

use Cake\Database\Type;
use Cake\I18n\Time;
use Cake\Utility\Inflector;
use Cake\Utility\Security;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;

trait JwtTrait {

    protected $_jwtDefaultAlgorithm = 'HS256';

    private static $_namespaceMap = ['HS' => 'Hmac', 'RS' => 'Rsa', 'ES' => 'Ecdsa'];
    private static $_allowedHashes = ['256', '384', '512'];

    private static $_claims = [
        'timestamp' => ['expiration', 'issued_at', 'not_before'],
        'string' => ['audience', 'id', 'issuer', 'subject']
    ];

    /**
     * creates a new JWT token
     *
     * @param array $data the data to be set (inclusive registered claims)
     * @param string $algorithm the sign algorithm to be used (false for unsigned token)
     * @return Token the generated token
     */
    public function generateToken (array $data, $algorithm = null) {
        $builder = new Builder();
        foreach (array_keys(static::$_claims) as $type) {
            foreach (static::$_claims[$type] as $claim) {
                if (isset($data[$claim])) {
                    if ($type == 'timestamp') {
                        $data[$claim] = (new Time($data[$claim]))->timestamp;
                    }
                    $builder->{'set' . Inflector::camelize($claim)}($data[$claim]);
                    unset($data[$claim]);
                }
            }
        }
        foreach ($data as $name => $value) {
            $builder->set($name, $value);
        }
        if ($algorithm !== false) {
            $builder->sign($this->_getAlgorithmClass($algorithm), Security::salt());
        }
        return $builder->getToken();
    }

    private function _getAlgorithmClass ($algorithm) {
        $algorithm = strtoupper($algorithm ?: $this->_jwtDefaultAlgorithm);
        $namespace = substr($algorithm, 0, 2);
        $class = (int) substr($algorithm, 2);
        if (!in_array($class, static::$_allowedHashes)) {
            throw new \RuntimeException ('illegal algorithm');
        }
        if (!isset(static::$_namespaceMap[$namespace])) {
            throw new \RuntimeException ('illegal algorithm');
        }
        $className = sprintf('Lcobucci\JWT\Signer\%s\Sha%s', static::$_namespaceMap[$namespace], $class);
        return new $className;
    }

    /**
     * parses a token from an encoded string
     *
     * @param string $token the encoded token string
     * @param string|bool $verify the algorithm the signature should be verified with (defaults to jwtDefaultAlgorithm; false to skip verify)
     * @param array|bool $validate the extra data to validate (timestamp claims will be validated automatically; false to skip validate)
     * @return Token|false the parsed token or false if something failed
     */
    public function parseToken ($token, $verify = null, $validate = []) {
        try {
            $token = (new Parser())->parse($token);
        } catch (\Exception $e) {
            return false;
        }
        if ($verify !== false) {
            if (!$token->verify($this->_getAlgorithmClass($verify), Security::salt())) {
                return false;
            }
        }
        if ($validate !== false) {
            $data = new ValidationData();
            foreach (static::$_claims['string'] as $claim) {
                if (isset($validate[$claim])) {
                    $data->{'set' . Inflector::camelize($claim)}($validate[$claim]);
                }
            }
            if (!$token->validate($data)) {
                return false;
            }
        }
        return $token;
    }

}