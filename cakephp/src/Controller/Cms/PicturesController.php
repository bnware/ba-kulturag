<?php
namespace App\Controller\Cms;

use App\Controller\CmsController;
use App\Model\Entity\Picture;
use Cake\Core\Configure;
use Cake\Event\Event;

/**
 * Pictures Controller
 *
 * @property \App\Model\Table\PicturesTable $Pictures
 */
class PicturesController extends CmsController
{

    public function initialize () {
        parent::initialize();
        $this->Auth->getAuthorize('Acl.ObjectCrud')->mapActions(['upload' => 'create']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pictures = $this->Pictures->find();

        if ($this->request->is('ajax')) {
            $this->set('config', Configure::read('Pictures'));
            $this->set('_serialize', ['pictures', 'config']);
        } else {
            $this->paginate = ['sortWhitelist' => ['id', 'alt', 'width', 'height'], 'limit' => 20, 'order' => ['alt' => 'ASC']];
            $pictures = $this->paginate($this->Pictures);
        }
        $this->set(compact('pictures'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {}

    /**
     * Delete method
     *
     * @param string|null $id Picture id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $picture = $this->Pictures->get($id);
        if ($this->Pictures->delete($picture)) {
            self::_deletePictureFiles($id);
            $this->Flash->success('Das Bild wurde gelöscht.');
        } else {
            $this->Flash->error('Das Bild konnte nicht gelöscht werden. Bitte versuchen Sie es später noch einmal');
        }
        return $this->redirect(['action' => 'index']);
    }

    public function upload () {
        $this->request->allowMethod('ajax');
        $picture = $this->Pictures->newEntity();
        $picture = $this->Pictures->patchEntity($picture, $this->request->data);
        if ($this->Pictures->save($picture)) {
            self::_handleUploadedPicture($picture);
            $result = $picture->id;
        } else {
            $result = $picture->errors();
        }
        $this->response->body(json_encode($result));
        return $this->response;
    }

    private static function _handleUploadedPicture (Picture $picture) {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $original = imagecreatefromstring(file_get_contents($picture->file['tmp_name']));
        Picture::export($original, 'png', PICTURES . $picture->id);
        $base = Configure::read('App.imageBaseUrl');
        $config = Configure::read('Pictures.thumbnail');
        $thumbnail = Picture::scalePicture($original, $config['width'], $config['height'], true);
        if ($picture->gray) {
            $grayed = Picture::grayScale($thumbnail);
            imagedestroy($thumbnail);
            $thumbnail = $grayed;
            unset($grayed);
        }
        Picture::export($thumbnail, $config['format'], sprintf('%s%s%s/%u', WWW_ROOT, $base, $config['path'], $picture->id));
        imagedestroy($thumbnail);
        unset($thumbnail);
        $scale = function (array $config) use ($original, $picture, $base) {
            $width = @$config['width'] ?: PHP_INT_MAX;
            $height = @$config['height'] ?: PHP_INT_MAX;
            $scaled = Picture::scalePicture($original, $width, $height);
            Picture::copyright($scaled, date('Y'));
            Picture::export($scaled, $config['format'], sprintf('%s%s%s/%u', WWW_ROOT, $base, $config['path'], $picture->id));
            imagedestroy($scaled);
            return $scaled == $original;
        };
        if ($scale(Configure::read('Pictures.small'))) {
            return;
        }
        if ($scale(Configure::read('Pictures.medium'))) {
            return;
        }
        $scale(Configure::read('Pictures.large'));
    }

    private static function _deletePictureFiles ($id) {
        $config = Configure::read('Pictures');
        $base = Configure::read('App.imageBaseUrl');
        foreach (['thumbnail', 'small', 'medium', 'large'] as $type) {
            $file = sprintf('%s%s%s/%u.%s', WWW_ROOT, $base, $config[$type]['path'], $id, $config[$type]['format']);
            @unlink($file);
        }
        $file = sprintf('%s%u.png', PICTURES, $id);
        @unlink($file);
    }
}
