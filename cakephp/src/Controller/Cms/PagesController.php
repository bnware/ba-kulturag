<?php
namespace App\Controller\Cms;

use App\Controller\CmsController;

/**
 * Pages Controller
 *
 * @property \App\Model\Table\PagesTable $Pages
 */
class PagesController extends CmsController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentPages'],
            'sortWhitelist' => ['Acos.lft', 'Pages.header', 'Pages.label'],
            'limit' => 20,
            'order' => ['Acos.lft' => 'ASC']
        ];
        $pages = $this->paginate($this->Pages->find('sortable'));

        $this->set(compact('pages'));
        $this->set('_serialize', ['pages']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $page = $this->Pages->newEntity();
        if ($this->request->is('post')) {
            $page = $this->Pages->patchEntity($page, $this->request->data);
            if ($this->Pages->save($page)) {
                $this->Flash->success('Die Seite wurde gespeichert.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Die Seite konnte nicht gespeichert werden. Versuchen Sie es später noch einmal.');
            }
        }
        $parentPages = $this->Pages->find('path', ['limit' => 200]);
        $this->set(compact('page', 'parentPages'));
        $this->set('_serialize', ['page']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Page id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $page = $this->Pages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $page = $this->Pages->patchEntity($page, $this->request->data);
            if ($this->Pages->save($page)) {
                $this->Flash->success('Die Seite wurde gespeichert.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Die Seite konnte nicht gespeichert werden. Versuchen Sie es später noch einmal.');
            }
        }
        $parentPages = $this->Pages->find('path', ['limit' => 200, 'filter' => $id]);
        $this->set(compact('page', 'parentPages'));
        $this->set('_serialize', ['page']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Page id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $page = $this->Pages->get($id);
        if ($this->Pages->delete($page)) {
            $this->Flash->success('Die Seite wurde gelöscht.');
        } else {
            $this->Flash->error('Die Seite konnte nicht gelöscht werden. Möglicherweise hat sie noch Unterseiten.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
