<?php
namespace App\Controller\Cms;

use App\Controller\CmsController;
use App\Utility\JwtTrait;
use Cake\Controller\Component\AuthComponent;
use Cake\Database\Connection;
use Cake\Database\Type;
use Cake\Event\Event;
use Cake\Mailer\MailerAwareTrait;
use Lcobucci\JWT\Token;

/**
 * Members Controller
 *
 * @property \App\Model\Table\MembersTable $Members
 */
class MembersController extends CmsController {

    protected $_passwordAction = 'password';

    use MailerAwareTrait, JwtTrait;

    public function initialize () {
        parent::initialize();
        $this->Auth->allow(['resetPassword', 'logout']);
    }

    public function beforeFilter (Event $event) {
        parent::beforeFilter($event);
        if ($this->request->action == 'password') {
            $config = ['Jwt' => ['fields' => ['username' => 'id']]];
            $config += $this->Auth->config('authenticate');
            $this->Auth->config('authenticate', $config, false);
            if (isset($this->request->pass[0])) {
                $this->Auth->config('authError', 'Dieser Link ist nicht mehr gültig.');
                $this->Auth->logout();
            }
        }
    }

    public function isAuthorized($user = null) {
        if ($this->_isPasswordAction()) {
            return true;
        }
        parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index () {
        $this->paginate = ['sortWhitelist' => ['fname', 'lname', 'email', 'show_email'], 'limit' => 20, 'order' => ['lname' => 'ASC']];
        $this->set('members', $this->paginate($this->Members->find('all', ['contain' => 'Roles'])));
        $this->set('_serialize', ['members']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add () {
        $member = $this->Members->newEntity();
        if ($this->request->is('post')) {
            list($data, $special) = $this->_filterSpecial(['cms', 'chk_show_email']);
            if ($special['cms']) {
                $data['password'] = $this->_generatePassword();
                $data['ch_password'] = true;
            }
            $member = $this->Members->patchEntity($member, $data);
            if ($this->Members->save($member)) {
                if ($special['cms']) {
                    $this->getMailer('Member')->send('accountCreated', [$member, $data['password']]);
                }
                $this->Flash->success('Das Mitglied wurde erfolgreich angelegt.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Das Mitglied konnte nicht angelegt werden. Bitte versuchen Sie es später noch einmal.');
            }
        }
        $roles = $this->Members->Roles->find('listAllowed', ['aro' => ['Members' => $this->Auth->user()], 'Acl' => $this->Acl, 'action' => 'create']);
        $this->set(compact('member', 'roles'));
        $this->set('_serialize', ['member']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Member id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit ($id = null) {
        $member = $this->Members->get($id, ['contain' => ['Roles']]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            list($data, $special) = $this->_filterSpecial(['cms', 'chk_show_email']);
            if (!$special['chk_show_email']) {
                // explicitly unset 'show_email'
                $data['show_email'] = null;
            }
            if ($special['cms']) {
                if (!isset($member->password)) {
                    $mail = 'accountCreated';
                    $data['password'] = $this->_generatePassword();
                    $data['ch_password'] = true;
                }
            } else {
                if (isset($member->password)) {
                    $mail = 'accountClosed';
                }
                // explicitly unset 'show_email'
                $data['password'] = null;
                // explicitly disable enquiries
                $data['enquiries'] = false;
            }
            $member = $this->Members->patchEntity($member, $data);
            if ($this->Members->save($member)) {
                if (isset($mail)) {
                    $this->getMailer('Member')->send($mail, [$member, $data['password']]);
                }
                $this->Flash->success('Das Mitglied wurde erfolgreich geändert');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Das Mitglied konnte nicht geändert werden.');
            }
        }
        $roles = $this->Members->Roles->find('listAllowed', ['aro' => ['Members' => $this->Auth->user()], 'Acl' => $this->Acl]);
        $this->set(compact('member', 'roles'));
        $this->set('_serialize', ['member']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Member id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete ($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $member = $this->Members->get($id);
        if ($this->Members->delete($member)) {
            if ($member->cms) {
                $this->getMailer('Member')->send('accountClosed', [$member]);
            }
            $this->Flash->success('Das Mitglied wurde gelöscht.');
        } else {
            $this->Flash->error('Das Mitglied konnte nicht gelöscht werden. Bitte versuchen Sie es später noch einmal.');
        }
        return $this->redirect(['action' => 'index']);
    }

    public function login () {
        $this->viewBuilder()->layout('login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $redirectUrl = $this->Auth->redirectUrl();
                if ($user['ch_password']) {
                    $this->request->session()->write('Password.redirect', $redirectUrl);
                    $redirectUrl = ['action' => 'password', $this->generateToken(['member' => $user['id']])];
                } else {
                    $this->Auth->setUser($user);
                }
                return $this->redirect($redirectUrl);
            }
            $this->Flash->error('Email oder Passwort sind inkorrekt', ['key' => 'auth']);
        }
    }

    public function logout () {
        $this->redirect($this->Auth->logout());
    }

    /**
     * @param Token $token
     * @return \Cake\Network\Response|null|void
     */
    public function password ($token = null) {
        $member = $this->Members->get($this->Auth->user('id'), ['contain' => ['Roles']]);
        if (isset($token) && !$member->ch_password && !$token->getClaim('reset', false)) {
            $this->Flash->error('Sie haben diesen Link bereits benutzt um Ihr Password zu ändern.');
            return $this->redirect($this->Auth->logout());
        }
        $this->set(compact('token', 'member'));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $old = $member->password;
            if (!$token && !$this->Auth->identify()) {
                return $this->Flash->error('Das aktuelle Passwort ist nicht korrekt');
            }
            if ($this->request->data('new') != $this->request->data('wdh')) {
                $this->Flash->error('Die Passwortwiederholung stimmt nicht mit dem Passwort überein');
            } else {
                $data = [
                    'password' => $this->request->data('new'),
                    'ch_password' => false
                ];
                $this->Members->patchEntity($member, $data);
                if ($member->password == $old) {
                    $this->Flash->error('Das neue Passwort muss sich von dem vorherigen unterscheiden');
                } else {
                    if ($this->Members->save($member)) {
                        $this->Flash->success('Das Passwort wurde erfolgreich geändert.');
                        $redirectUrl = $this->request->session()->read('Password.redirect');
                        if (empty($redirectUrl)) {
                            $redirectUrl = $this->Auth->redirectUrl();
                        }
                        return $this->redirect($redirectUrl);
                    } else {
                        $this->Flash->error('Das Passwort konnte nicht geändert werden.');
                    }
                }
            }
        }
        if ($token) {
            $this->Auth->logout();
            $this->viewBuilder()->layout('login');
        }
    }

    public function resetPassword () {
        $this->viewBuilder()->layout('login');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $member = $this->Members->find('login', ['email' => $this->request->data('email')])->first();
            if (empty($member)) {
                sleep(1);
            } else {
                $this->getMailer('Member')->send('passwordReset', [$member]);
            }
            $this->Flash->success('Die Email wurde versandt.');
            $this->redirect(['action' => 'login']);
        }
    }

    private function _filterSpecial ($fields) {
        $fields = array_flip($fields);
        $data = array_diff_key($this->request->data, $fields);
        $special = array_intersect_key($this->request->data, $fields);
        $converter = Type::build('boolean');
        $driver = $this->Members->connection()->driver();
        $special = array_map(function ($value) use ($converter, $driver) {
            return $converter->toPHP($value, $driver);
        }, $special);
        return [$data, $special];
    }

    private function _generatePassword ($length = 10, $chars = 'aA0') {
        $chars = preg_replace('/([^b-z]*)a([^b-z]*)/', '\1abcdefghijklmnopqrstuvwxyz\2', $chars);
        $chars = preg_replace('/([^B-Z]*)A([^B-Z]*)/', '\1ABCDEFGHIJKLMNOPQRSTUVWXYZ\2', $chars);
        $chars = preg_replace('/([^1-9]*)0([^1-9]*)/', '0123456789\1\2', $chars);
        $chars = str_split($chars);
        $password = "";
        for ($i = 0; $i < $length; $i++) {
            $random = rand(0, count($chars) - 1);
            $char = $chars[$random];
            $password .= $char;
        }
        return $password;
    }
}
