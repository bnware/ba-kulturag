<?php

namespace App\Controller;

use Cake\Controller\Component\AuthComponent;
use Cake\Event\Event;

/**
 * Class CmsController
 *
 * @package App\Controller
 * @property \Acl\Controller\Component\AclComponent $Acl
 */
class CmsController extends AppController {

    protected $_passwordAction = null;

    public function initialize () {
        parent::initialize();
        $this->loadComponent('Acl.Acl');
        $this->viewBuilder()->helpers([
            'Acl' => [
                'component' => $this->Acl
            ]
        ]);
        $this->loadComponent('Auth', [
            'authenticate' => [
                AuthComponent::ALL => [
                    'userModel' => 'Members',
                    'finder' => 'login',
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ],
                'Form' => [
                    'passwordHasher' => [
                        'className' => 'Weak',
                        'hashType' => 'sha1'
                    ],
                ]
            ],
            'loginAction' => [
                'prefix' => 'cms',
                'controller' => 'Members',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'prefix' => 'cms',
                'controller' => 'Pages',
                'action' => 'index'
            ],
            'authError' => 'Sie sind nicht berechtigt auf diese Resource zuzugreifen.',
            'authorize' => [
                'Controller',
                'Acl.ObjectCrud' => [
                    'userModel' => 'Members'
                ]
            ]
        ]);
    }

    public function beforeFilter (Event $event) {
        if (!$this->_isPasswordAction()) {
            $this->request->session()->write('Password.redirect', $this->request->here(false));
        }
        if (empty($this->Auth->user())) {
            $this->Auth->config('authError', false);
        }
        return parent::beforeFilter($event);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        if ($user = $this->Auth->user()) {
            $this->set('user', $user);
        }
    }

    /**
     * checks if the current action is the password action
     *
     * @return bool
     */
    protected function _isPasswordAction () {
        return $this->_passwordAction == $this->request->param('action');
    }

    public function isAuthorized($user = null) {
        return false;
    }

}