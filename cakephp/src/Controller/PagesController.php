<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Acl\Model\Table\AcosTable;
use App\Model\Table\PagesTable;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @property AcosTable Acos
 * @property PagesTable Pages
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * displays the requested page
     *
     * @param string $path
     * @return \Cake\Network\Response|void
     * @throws \Exception
     */
    public function display ($path = '')
    {
        $this->loadModel('Acl.Acos');
        $this->viewBuilder()->helpers(['BBCode.BBCode']);
        try {
            $node = $this->Acos->node('ROOT/Pages/' . $path);
            if ($node === false) {
                throw new NotFoundException();
            }
            $this->set('page', $this->Pages->get($node->firstOrFail()->foreign_key));
        } catch (NotFoundException $e) {
            throw $e;
        } catch (\Exception $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
}
