<?php
namespace App\Shell;

use App\Model\Table\MembersTable;
use Cake\Console\Shell;

/**
 * Setup shell command.
 * @property MembersTable Members
 */
class SetupShell extends Shell
{

    public function getOptionParser () {
        $parser = parent::getOptionParser();
        $parser->description('Creates an initial admin user');
        $parser->addArgument('email', ['help' => 'the email used as login for the initial admin user', 'required' => true]);
        return $parser;
    }

    /**
     * main() method.
     *
     * @param string $email the email address to use
     * @return bool|int Success or error code.
     */
    public function main($email)
    {
        $this->loadModel('Members');
        $password = $this->in('Password:');
        $data = ['role_id' => 1, 'fname' => 'initial', 'lname' => 'admin', 'ch_password' => false] + compact('email', 'password');
        $member = $this->Members->newEntity($data);
        if ($this->Members->save($member)) {
            $this->success('initial admin user successfully created');
            return true;
        } else {
            $this->err('failed to create initial admin user');
            foreach ($member->errors() as $field => $error) {
                foreach ($error as $message) {
                    $this->err(sprintf('%s: %s', $field, $message));
                }
            }
        }
    }
}
