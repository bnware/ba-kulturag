<?php
namespace App\Mailer;

use App\Model\Entity\Member;
use App\Utility\JwtTrait;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\Mailer\Mailer;
use Cake\Utility\Security;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

/**
 * Member mailer.
 */
class MemberMailer extends Mailer
{

    use JwtTrait;

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'Member';

    public function __construct ($email) {
        parent::__construct($email);
        $this->emailFormat('both');
        $this->helpers(['Html', 'Url']);
    }

    /**
     * sends the login data for new CMS account
     *
     * @param Member $member
     * @param string $password
     */
    public function accountCreated ($member, $password) {
        $subject = 'Kultur AG CMS - Account angelegt';
        $data = [
            'expiration' => '+1month',
            'member' => $member->id
        ];
        $token = $this->generateToken($data);
        $this
            ->to($member->email, $member->name)
            ->subject($subject)
            ->set('name', $member->name)
            ->set('email', $member->email)
            ->set('password', $password)
            ->set('title', $subject)
            ->set('token', $token);
    }

    /**
     * sends notification for closed account
     *
     * @param Member $member
     */
    public function accountClosed ($member) {
        $subject = 'Kultur AG CMS - Account geschlossen';
        $this
            ->to($member->email, $member->name)
            ->subject($subject)
            ->set('name', $member->name)
            ->set('email', $member->email)
            ->set('title', $subject);
    }

    /**
     * sends the login data for new CMS account
     *
     * @param Member $member
     */
    public function passwordReset($member) {
        $subject = 'Kultur AG CMS - Passwort zurücksetzen';
        $data = [
            'expiration' => '+1hour',
            'member' => $member->id,
            'reset' => true
        ];
        $token = $this->generateToken($data);
        $this
            ->to($member->email, $member->name)
            ->subject($subject)
            ->set('name', $member->name)
            ->set('email', $member->email)
            ->set('title', $subject)
            ->set('token', $token);
    }
}
