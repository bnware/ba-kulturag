<?php

namespace App\Auth;

use App\Utility\JwtTrait;
use Cake\Auth\BaseAuthenticate;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Utility\Security;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;

class JwtAuthenticate extends BaseAuthenticate {

    use JwtTrait;

    /**
     * Authenticate a user based on the request information.
     *
     * @param \Cake\Network\Request $request Request to get authentication information from.
     * @param \Cake\Network\Response $response A response object that can have headers added.
     * @return mixed Either false on failure, or an array of user data on success.
     */
    public function authenticate (Request $request, Response $response) {
        return $this->getUser($request);
    }

    public function getUser (Request $request) {
        if (!$this->_checkToken($request)) {
            return false;
        }
        $token = $request->param('pass')[0];
        $token = $this->parseToken($token);
        if (!$token) {
            return false;
        }
        $request->param('pass', [$token]);
        $result = $this->_query($token->getClaim('member'))->first();
        if (empty($result)) {
            return false;
        }
        $result->unsetProperty($this->_config['fields']['password']);
        return $result->toArray();
    }


    private function _checkToken (Request $request) {
        $passed = $request->param('pass');
        if (count($passed) != 1) {
            return false;
        }
        return !is_numeric($passed[0]);
    }
}