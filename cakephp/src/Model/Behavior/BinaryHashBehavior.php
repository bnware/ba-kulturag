<?php
namespace App\Model\Behavior;

use Cake\Database\Expression\FunctionExpression;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\Table;

/**
 * BinaryHash behavior
 */
class BinaryHashBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields' => []
    ];

    public function initialize (array $config) {
        parent::initialize($config);
        $fields = (array) $this->config('fields');
        foreach ($fields as $field) {
            $this->_table->schema()->columnType($field, 'string');
        }
    }

    public function beforeSave (Event $event, EntityInterface $entity) {
        $fields = (array) $this->config('fields');
        foreach ($fields as $field) {
            if (!$entity->dirty($field)) {
                continue;
            }
            $value = $entity->get($field);
            if (is_null($value)) {
                continue;
            }
            $entity->set($field, hex2bin($value), ['setter' => false, 'guard' => false]);
        }
    }

    public function beforeFind (Event $event, Query $query, $options, $primary) {
        $fields = (array) $this->config('fields');
        $aliased = [];
        $defaultAlias = $this->_table->alias();
        foreach ($fields as $field) {
            $aliased += $query->aliasField($field, $defaultAlias);
        }
        $selected = $query->clause('select');
        $fields += $aliased;
        foreach ($aliased as $alias => $field) {
            foreach (array_keys($selected, $field) as $key) {
                $fields[$key] = $field;
            }
        }
        $query->formatResults(function (ResultSetInterface $results) use ($fields) {
            return $results->map(function ($row) use ($fields) {
                $replaced = [];
                foreach ($fields as $alias => $field) {
                    $this->_formatHash($row, $alias, $replaced);
                    $this->_formatHash($row, $field, $replaced);
                }
                return $row;
            });
        });
    }

    /**
     * @param EntityInterface $row
     * @param $field
     * @param $replaced
     * @internal param $options
     */
    private function _formatHash ($row, $field, &$replaced) {
        if (!in_array($field, $replaced) && isset($row[$field])) {
            $value = bin2hex($row[$field]);
            if ($row instanceof EntityInterface) {
                $row->set($field, $value, ['setter' => false, 'guard' => false]);
                $row->dirty($field, false);
            } else {
                $row[$field] = $value;
            }
            $replaced[] = $field;
        }
    }
}