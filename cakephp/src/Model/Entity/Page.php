<?php
namespace App\Model\Entity;

use Cake\Collection\Iterator\MapReduce;
use Cake\ORM\Locator\LocatorAwareTrait;

/**
 * Page Entity.
 *
 * @property int $id
 * @property int $parent_id
 * @property \App\Model\Entity\Page $parent_page
 * @property string $name
 * @property string $header
 * @property string $label
 * @property string $content
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Page[] $child_pages
 * @property \App\Model\Entity\Project[] $projects
 * @property-read string alias
 */
class Page extends AclEntity
{

    use LocatorAwareTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getAlias () {
        $table = $this->tableLocator()->get($this->source());
        $path = $table->Aco
            ->node($this)
            ->mapReduce(function ($node, $key, MapReduce $mapReduce) {
                $mapReduce->emitIntermediate($node['alias'], 'path');
            }, function ($nodes, $bucket, MapReduce $mapReduce) {
                $mapReduce->emit(implode('/', array_reverse($nodes)));
            });
        return $path->all()->first();
    }

    public function parentNode () {
        return isset($this->parent_page) ? $this->parent_page->alias : parent::parentNode();
    }
}
