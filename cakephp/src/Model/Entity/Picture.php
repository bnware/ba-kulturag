<?php
namespace App\Model\Entity;

use Cake\Core\Configure;

/**
 * Picture Entity.
 *
 * @property int $id
 * @property string $alt
 * @property int $width
 * @property int $height
 * @property \Cake\I18n\Time $modified
 * @property array $file
 * @property boolean gray
 */
class Picture extends AclEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * resizes the given gd2 resource to fit into the given rectangle without changing the aspect ratio
     *
     * @param resource $img the image to scale
     * @param int $maxWidth the width to fit in
     * @param int $maxHeight the height to fit in
     * @param bool $forceNew whether to force the creation of a new resource
     * @return resource the resized
     */
    public static function scalePicture ($img, $maxWidth, $maxHeight, $forceNew = false) {
        $width = imagesx($img);
        $height = imagesy($img);
        if ($width <= $maxWidth && $height <= $maxHeight) {
            if ($forceNew) {
                $scaled = imagecreatetruecolor($width, $height);
                imagecopy($scaled, $img, 0, 0, 0, 0, $width, $height);
                return $scaled;
            } else {
                return $img;
            }
        }
        $scaledWidth = $maxWidth;
        $scaledHeight = $maxHeight;
        $scaleX = $width / doubleval($maxWidth);
        $scaleY = $height / doubleval($maxHeight);
        if ($scaleX > $scaleY) {
            $scaledHeight = round($height / $scaleX);
        } elseif ($scaleY > $scaleX) {
            $scaledWidth = round($width / $scaleY);
        }
        $scaled = imagecreatetruecolor($scaledWidth, $scaledHeight);
        imagecopyresampled($scaled, $img, 0, 0, 0, 0, $scaledWidth, $scaledHeight, $width, $height);
        return $scaled;
    }

    /**
     * @param resource $img the image to watermark
     * @param int $year the year to write
     */
    public static function copyright ($img, $year) {
        $config = Configure::read('Pictures.copyright');
        $text = sprintf("&#xA9; KulturAG %u", $year);
        $ttf = WWW_ROOT . $config['font'];
        $box = imagettfbbox($config['size'], 0, $ttf, $text);
        $width = abs($box[4] - $box[0]) + 2 * $config['padding'];
        $height = abs($box[5] - $box[1]) + 2 * $config['padding'];
        $copyright = imagecreatetruecolor($width, $height);
        $black = imagecolorallocate($copyright, 0, 0, 0);
        $white = imagecolorallocate($copyright, 255, 255, 255);
        imagefill($copyright, 0, 0, $black);
        imagettftext($copyright, $config['size'], 0, $config['padding'], $height - $config['padding'], $white, $ttf, $text);
        imagecopymerge($img, $copyright, imagesx($img) - $width, imagesy($img) - $height, 0, 0, $width, $height, 50);
        imagedestroy($copyright);
    }

    public static function grayScale ($img) {
        $width = imagesx($img);
        $height = imagesy($img);
        $grayed = imagecreatetruecolor($width, $height);
        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $col = imagecolorsforindex($img, imagecolorat($img, $x, $y));
                $gray = $col['red'] * 0.15 + $col['green'] * 0.5 + $col['blue'] * 0.35;
                imagesetpixel($grayed, $x, $y, imagecolorallocatealpha($grayed, $gray, $gray, $gray, $col['alpha']));
            }
        }
        return $grayed;
    }

    public static function export ($img, $format, $file = null, $quality = null) {
        if (!isset($quality)) {
            $quality = $format == 'png' ? 4 : 100;
        }
        if (isset($file)) {
            @mkdir(dirname($file), 0755, true);
            $file = sprintf('%s.%s', $file, $format);
        }
        imageinterlace($img, 1);
        ob_start();
        switch ($format) {
            case 'jpg':
            case 'jpeg':
                imagejpeg($img, $file, $quality);
                break;
            case 'png':
                imagepng($img, $file, $quality);
                break;
            case 'gif':
                imagegif($img, $file);
                break;
        }
        return ob_get_clean();
    }

    protected function _setFile ($value) {
        $data = getimagesize($value['tmp_name']);
        $this->width = $data[0];
        $this->height = $data[1];
        if (empty($this->alt)) {
            $this->alt = $value['name'];
        }
        return $value;
    }
}
