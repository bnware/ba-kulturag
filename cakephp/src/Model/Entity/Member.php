<?php
namespace App\Model\Entity;

use Cake\Auth\WeakPasswordHasher;
use Cake\ORM\Entity;

/**
 * Member Entity.
 *
 * @property int $id
 * @property string $fname
 * @property string $lname
 * @property int $avatar_id
 * @property \App\Model\Entity\Picture $avatar
 * @property string $email
 * @property int $role_id
 * @property \App\Model\Entity\Role $role
 * @property string $password
 * @property bool $ch_password
 * @property string $show_email
 * @property bool $enquiries
 * @property \Cake\I18n\Time $modified
 * @property \Acl\Model\Entity\Aro[] $aro
 * @property \Acl\Model\Entity\Aco[] $aco
 */
class Member extends AclEntity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected $_virtual = ['name'];

    protected function _setPassword ($value) {
        return is_null($value) ? null : (new WeakPasswordHasher(['hashType' => 'sha1']))->hash($value);
    }

    protected function _getName () {
        return $this->fname . ' ' . $this->lname;
    }

    protected function _getProfileEmail () {
        return $this->_getChkShowEmail() ? $this->show_email : $this->email;
    }

    protected function _getCms () {
        return isset($this->password);
    }

    protected function _getChkShowEmail () {
        return isset($this->show_email);
    }

    public function parentNode () {
        return parent::parentNode() . '/' . $this->role->name;
    }
}
