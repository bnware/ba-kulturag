<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class AclEntity extends Entity {

    public function parentNode () {
        return 'ROOT/' . $this->source();
    }

}