<?php
namespace App\Model\Table;

use App\Model\Entity\Page;
use BBCode\Validation\BBCodeValidator;
use Cake\Collection\Iterator\MapReduce;
use Cake\Database\Expression\IdentifierExpression;
use Cake\Datasource\EntityInterface;
use Cake\Datasource\ResultSetInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentPages
 * @property \Cake\ORM\Association\HasMany $ChildPages
 * @property \Cake\ORM\Association\HasMany $Projects
 */
class PagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pages');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Acl.Acl', ['type' => 'Controlled', 'alias' => true]);

        $this->belongsTo('ParentPages', [
            'className' => 'Pages',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildPages', [
            'className' => 'Pages',
            'foreignKey' => 'parent_id',
            'dependent' => false,
        ]);
//        $this->hasMany('Projects', [
//            'foreignKey' => 'page_id'
//        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('header', 'create')
            ->notEmpty('header');

        $validator
            ->requirePresence('label', 'create')
            ->notEmpty('label');

        $validator
            ->provider('BBCode', 'BBCode\Validation\BBCodeValidator')
            ->requirePresence('content', 'create')
            ->add('content', 'BBCode', [
                'provider' => 'BBCode',
                'rule' => 'validate'
            ])
            ->allowEmpty('content');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentPages'));
        $rules->addDelete(function (EntityInterface $entity, $options) {
            $count = $this->find('all', ['fields' => 'id', 'conditions' => ['parent_id' => $entity->id]])->count();
            return $count == 0;
        });
        return $rules;
    }

    public function beforeSave(Event $event, EntityInterface $entity, $options) {
        if (isset($entity->parent_id) && !isset($entity->parent_page)) {
            $parent = $this->get($entity->parent_id);
            $entity->parent_page = $parent;
            $entity->dirty('parent_page', false);
        }
    }

    public function findPath (Query $query, $options) {
        $filter = '';
        if (isset($options['filter'])) {
            $filter = $this->get($options['filter'])->alias;
        }
        return $query
            ->select(['id'], true)
            ->mapReduce(function ($entity, $key, MapReduce $mapReduce) use ($filter) {
                if (!$entity instanceof Page) {
                    $class = $this->entityClass();
                    $entity = new $class($entity);
                }
                $alias = $entity->alias;
                if (empty($filter) || !preg_match(sprintf('/^%s/', preg_quote($filter, '/')), $alias)) {
                    $mapReduce->emit(str_replace('ROOT/Pages', '', $alias), $entity->id);
                }
            }, function () {});
    }

    public function findMenu (Query $query, $options) {
        $options += ['filter' => [], 'current' => ''];
        return $query
            ->select(['id', 'label'], true)
            ->where(['Pages.parent_id IS' => null])
            ->innerJoin('acos', ['Acos.model' => 'Pages', 'Acos.foreign_key' => new IdentifierExpression('Pages.id')])
            ->order('Acos.lft')
            ->mapReduce(function ($entity, $key, MapReduce $mapReduce) use ($options) {
                if (!$entity instanceof Page) {
                    $class = $this->entityClass();
                    $entity = new $class($entity);
                }
                $path = str_replace('ROOT/Pages/', '', $entity->alias);
                if (!in_array($path, $options['filter'])) {
                    $mapReduce->emit(['url' => ['controller' => 'Pages', 'action' => 'display', $path], 'active' => ($options['current'] == $path)], $entity->label);
                }
            }, function () {});
    }

    public function findSortable (Query $query, $options) {
        return $query->innerJoin('acos', ['Acos.model' => 'Pages', 'Acos.foreign_key' => new IdentifierExpression('Pages.id')]);
    }
}
