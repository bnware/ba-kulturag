<?php
namespace App\Model\Table;

use App\Model\Entity\Role;
use Cake\Collection\Iterator\MapReduce;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Roles Model
 *
 */
class RolesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('roles');
        $this->displayField('name');
        $this->primaryKey('id');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->numeric('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    public function findListAllowed (Query $query, $options) {
        $options += ['action' => '*'];
        return $query->find('list')
            ->mapReduce(function ($row, $key, MapReduce $mapReduce) use ($options) {
                if ($options['Acl']->check($options['aro'], 'ROOT/Members/' . $row['name'], $options['action'])) {
                    $mapReduce->emit($row);
                }
            }, function () {});
    }
}
