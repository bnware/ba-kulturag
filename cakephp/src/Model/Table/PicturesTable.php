<?php
namespace App\Model\Table;

use App\Model\Entity\Picture;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pictures Model
 *
 * @property \Cake\ORM\Association\HasMany $Awards
 */
class PicturesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('pictures');
        $this->displayField('alt');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Acl.Acl', ['Controlled']);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->numeric('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('alt', 'create')
            ->allowEmpty('alt');

        $validator
            ->numeric('width')
            ->notEmpty('width');

        $validator
            ->numeric('height')
            ->notEmpty('height');

        $validator
            ->uploadedFile('file', ['types' => ['image/jpeg', 'image/gif', 'image/png']], 'Der Upload ist fehlgeschlagen. Das Bild muss im JPEG-, GIF- oder PNG-Format vorliegen.');

        return $validator;
    }
}
