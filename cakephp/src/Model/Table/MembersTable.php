<?php
namespace App\Model\Table;

use App\Model\Entity\Member;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Members Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Pictures
 * @property \Cake\ORM\Association\BelongsTo $Roles
 */
class MembersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('members');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('BinaryHash', ['fields' => 'password']);
        $this->addBehavior('Timestamp');
        $this->addBehavior('Acl.Acl', ['type' => 'both', 'alias' => true]);

        $this->belongsTo('Pictures', [
            'foreignKey' => 'avatar_id',
            'propertyName' => 'avatar'
        ]);
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->numeric('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('fname', 'create', 'Der Vorname darf nicht leer sein.')
            ->notEmpty('fname', 'Der Vorname darf nicht leer sein.');

        $validator
            ->requirePresence('lname', 'create', 'Der Nachname darf nicht leer sein.')
            ->notEmpty('lname', 'Der Nachname darf nicht leer sein.');

        $validator
            ->numeric('avatar')
            ->allowEmpty('avatar');

        $validator
            ->email('email', false, 'Sie müssen eine gültige Email-Adresse eingeben.')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'Die Email-Adresse ist bereits vergeben.']);

        $validator
            ->requirePresence('role_id', 'create')
            ->numeric('role_id')
            ->notEmpty('role_id');

        $validator
            ->minLength('password', 8, 'Das Passwort muss mindestens 8 Stellen haben.')
            ->allowEmpty('password');

        $validator
            ->boolean('ch_password')
            ->notEmpty('ch_password');

        $validator
            ->email('show_email', false, 'Sie müssen eine gültige Email-Adresse eingeben.')
            ->allowEmpty('show_email');

        $validator
            ->boolean('enquiries')
            ->notEmpty('enquiries');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email'], 'Die Email-Adresse ist bereits vergeben.'));
        //TODO: activate existsIn check rule
//        $rules->add($rules->existsIn(['avatar_id'], 'Pictures'));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        return $rules;
    }

    public function findLogin (Query $query, array $options) {
        return $query->where(['password IS NOT' => null]);
    }

    public function findEnquiries (Query $query, array $options) {
        return $query->select('email', true)->where(['Members.enquiries' => 1]);
    }

    public function beforeSave (Event $event, EntityInterface $entity) {
        if ($entity->dirty('role_id')) {
            $entity->set('role', $this->Roles->get($entity->get('role_id')));
        }
    }
}
